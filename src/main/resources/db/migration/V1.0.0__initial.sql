insert into password_store(password) value('EMPTY');
insert into password_store(password) value('password');
insert into password_store(password) value('password');
insert into userinfo(email,first_name,last_name,phone_number,version,roles,password_id)
    value('anonymousUser','anonymousUser', 'anonymousUser','00000000000',1, 'ROLE_ANONYMOUS',1);
insert into userinfo(email,first_name,last_name,phone_number,version,roles,password_id)
    value('john','john', 'lastname','13590000001',1,'ROLE_USER',2);
insert into userinfo(email,first_name,last_name,phone_number,version,roles,password_id)
    value('mary','mary', 'lastmary','13590000002',1,'ROLE_ADMIN,ROLE_USER',3);

insert into product(name,status,description,image_src,price,category,version,created_date)
    value('Product','NORMAL','product Desc', '../assets/patterns/3.png','10000','CAT1',1, UTC_TIMESTAMP);
insert into product(name,status,description,image_src,price,category,version,created_date)
    value('Product2','NORMAL','product Desc2', '../assets/patterns/3.png','20000','CAT1',1, UTC_TIMESTAMP);
insert into product(name,status,description,image_src,price,category,version,created_date)
    value('Product3','NORMAL','product Desc3', '../assets/patterns/3.png','30000','CAT2',1, UTC_TIMESTAMP);