package oracle.apps.fltp.backend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationPid;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Primary;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;

/**
 * Created by ecchang on 2016/10/19
 * Server starter
 */
@SpringBootApplication(scanBasePackages = {"szu.example", "oracle.apps.fltp.backend"})
@ImportResource(value = "classpath:META-INF/apps-context.xml")
public class ServerStarter {
  private static Logger logger = LoggerFactory.getLogger(ServerStarter.class);

  @Bean
  @Primary
  @ConfigurationProperties("spring.datasource")
  public DataSource primaryDataSource() {
    return DataSourceBuilder.create().build();
  }

  @PostConstruct
  private void handlePid() throws IOException {
    File file = new File("application.pid");
    new ApplicationPid().write(file);
    file.deleteOnExit();
  }

  public static void main(String[] args) {
    SpringApplication.run(ServerStarter.class, args);
  }



  @Bean
  public WebMvcConfigurer corsConfigurer() {

    return new WebMvcConfigurerAdapter() {

      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedOrigins("*")
                .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
                .allowedHeaders("*");
      }
    };
  }

  /**
   * when using Spring Data REST + Spring Framework 4.2, only HandlerMapping instances created by
   * Spring MVC WebMvcConfigurationSupport and controllers annotated with @CrossOrigin will be CORS aware.
   * Spring Data REST still compiles against Spring Framework 4.1,
   * and does not support builtin Spring Framework CORS implementation.
   * Feel free to vote for the DATAREST-573 related issue. [NOT FIX at 2016/03/28]
   * https://jira.spring.io/browse/DATAREST-573
   * <p/>
   * Right now, the best solution is to use a filter based approach.
   *
   * @return
   */

  @Bean
  public CorsFilter corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.addAllowedMethod("OPTIONS");
    config.addAllowedMethod("HEAD");
    config.addAllowedMethod("GET");
    config.addAllowedMethod("PUT");
    config.addAllowedMethod("POST");
    config.addAllowedMethod("DELETE");
    config.addAllowedMethod("PATCH");
    source.registerCorsConfiguration("/**", config);
    return new CorsFilter(source);
  }

  @Bean
  public FilterRegistrationBean RegistrationCORSFilter(CorsFilter corsFilter){
    final FilterRegistrationBean bean = new FilterRegistrationBean(corsFilter);
    bean.setOrder(SecurityProperties.DEFAULT_FILTER_ORDER);
    return bean;
  }
}

