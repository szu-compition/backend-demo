package oracle.apps.fltp.backend.core.data.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Converter to convert from java.time.LocalDate to java.sql.Date and back.
 *
 * @author Eclair
 */
// Converter sample, no use now after switch to Hibernate 5
//@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {
  @Override
  public Timestamp convertToDatabaseColumn(LocalDateTime localDate) {
    return null == localDate ? null : Timestamp.valueOf(localDate);
  }

  @Override
  public LocalDateTime convertToEntityAttribute(Timestamp date) {
    return null == date ? null : date.toLocalDateTime();
  }
}