package oracle.apps.fltp.backend.core.security;

import oracle.apps.fltp.backend.domain.user.Userinfo;
import oracle.apps.fltp.backend.domain.user.UserinfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by ecchang on 2016/10/19
 * user Repository is also need Authorize.
 * {@link UserinfoRepository}
 */
@Service
public class AuthUserDetailsService implements UserDetailsService {

  @Autowired
  @Lazy // This is important
  private UserinfoRepository userinfoRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Userinfo user = userinfoRepository.findByEmail(username);
    return new UserinfoDetails(user);
  }

  public static class UserinfoDetails extends User {

    private Userinfo user;

    private Long userId;

    public Long getUserId() {
      return userId;
    }

    public Userinfo getUser() {
      return user;
    }

    public UserinfoDetails(Userinfo user) {
      super(user.getEmail(),
              user.getPassword().getPassword(),
              AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRoles()));
      this.user = user;
      if(user != null){
        userId = user.getId();
      }
    }
  }
}
