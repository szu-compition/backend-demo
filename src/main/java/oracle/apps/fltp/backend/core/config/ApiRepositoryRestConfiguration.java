package oracle.apps.fltp.backend.core.config;

import oracle.apps.fltp.backend.domain.product.Product;
import oracle.apps.fltp.backend.domain.user.Userinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Created by ecchang on 2016/10/19
 */

@Configuration
public class ApiRepositoryRestConfiguration extends RepositoryRestConfigurerAdapter{

  @Autowired
  private LocalValidatorFactoryBean validator;

  @Override
  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    config.exposeIdsFor(Product.class);
    config.exposeIdsFor(Userinfo.class);
    config.setReturnBodyOnUpdate(true);
    config.setReturnBodyOnCreate(true);
  }

  /**
   * Add the validator to spring data rest.
   * Register validator which aware Message Source provide more readable error message
   * with this, validators will not execute but not able convert message code
   */
  @Override
  public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
    validatingListener.addValidator("beforeSave", validator);
    validatingListener.addValidator("beforeCreate", validator);
  }



  /**
   * Needed for mapping exceptions in jackson that otherwise would get an ugly error message.
   */
  @Autowired
  private JsonMappingHandlerExceptionResolver jsonMappingHandlerExceptionResolver;

}
