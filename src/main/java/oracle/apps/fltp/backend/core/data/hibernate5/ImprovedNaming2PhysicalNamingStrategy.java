package oracle.apps.fltp.backend.core.data.hibernate5;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import java.io.Serializable;

/**
 * Created by ecchang on 2016/10/19
 */
public class ImprovedNaming2PhysicalNamingStrategy implements PhysicalNamingStrategy, Serializable {
  private ImprovedNamingStrategy strategy = new ImprovedNamingStrategy();

  @Override
  public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return name;
  }

  @Override
  public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return name;
  }

  @Override
  public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return Identifier.toIdentifier(strategy.tableName(name.getText()), name.isQuoted());
  }

  @Override
  public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return name;
  }

  @Override
  public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return Identifier.toIdentifier(strategy.columnName(name.getText()), name.isQuoted());
  }
}
