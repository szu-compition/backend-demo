package oracle.apps.fltp.backend.core.data.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeLinkDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

/**
 * Created by ecchang on 2016/10/19
 */

@Component
@RepositoryEventHandler
public class GlobalEventHandler {
  private static Logger logger = LoggerFactory.getLogger(GlobalEventHandler.class);

  @HandleBeforeSave
  public void handleAllSave(Object o) {
    logger.warn("event product name = {}", o.getClass().getName());
  }

  @HandleBeforeLinkDelete
  public void checkLinkDeletePermission(Object project) {
    logger.warn("event product name = {}",project.getClass().getName());
  }

  @HandleBeforeDelete
  public void checkDeletePermission(Object project) {
    logger.warn("event product name = {}",project.getClass().getName());
  }
}