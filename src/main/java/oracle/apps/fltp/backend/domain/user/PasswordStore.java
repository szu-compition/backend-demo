package oracle.apps.fltp.backend.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.ZonedDateTime;

/**
 * Created by ecchang on 2016/10/19
 */

@Entity
public class PasswordStore {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotEmpty
  @JsonIgnore
  private String password;

  private ZonedDateTime expireDate;

  @LastModifiedDate
  private ZonedDateTime LastModifiedDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public ZonedDateTime getExpireDate() {
    return expireDate;
  }

  public void setExpireDate(ZonedDateTime expireDate) {
    this.expireDate = expireDate;
  }

  public ZonedDateTime getLastModifiedDate() {
    return LastModifiedDate;
  }

  public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
    LastModifiedDate = lastModifiedDate;
  }
}
