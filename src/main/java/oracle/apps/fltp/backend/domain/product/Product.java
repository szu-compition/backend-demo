package oracle.apps.fltp.backend.domain.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * Created by ecchang on 2016/10/19
 * Entity of Product
 */

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product{
  public enum Status{
    NORMAL,OUT_OF_STOCK, NOT_FOR_SALE, OFF_THE_SHELVES
  }

  public Product(){}

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Version
  private Integer version;

  @NotEmpty
  @ReadOnlyProperty
  private String name;

  @NotNull
  @Enumerated(EnumType.STRING)
  private Status status;

  private String description;

  private String imageSrc;
  private Integer price;
  private String category;
  private ZonedDateTime effectiveDate;
  private ZonedDateTime endDate;

  private ZonedDateTime createdDate;
  private ZonedDateTime LastModifiedDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getImageSrc() {
    return imageSrc;
  }

  public void setImageSrc(String imageSrc) {
    this.imageSrc = imageSrc;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public ZonedDateTime getEffectiveDate() {
    return effectiveDate;
  }

  public void setEffectiveDate(ZonedDateTime effectiveDate) {
    this.effectiveDate = effectiveDate;
  }

  public ZonedDateTime getEndDate() {
    return endDate;
  }

  public void setEndDate(ZonedDateTime endDate) {
    this.endDate = endDate;
  }

  public ZonedDateTime getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(ZonedDateTime createdDate) {
    this.createdDate = createdDate;
  }

  public ZonedDateTime getLastModifiedDate() {
    return LastModifiedDate;
  }

  public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
    LastModifiedDate = lastModifiedDate;
  }
}
