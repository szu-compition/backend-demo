package oracle.apps.fltp.backend.domain.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeLinkDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

/**
 * Created by ecchang on 2016/10/19
 */

@Component
@RepositoryEventHandler(Product.class)
public class ProductEventHandler {

  private static Logger logger = LoggerFactory.getLogger(ProductEventHandler.class);
  @HandleBeforeSave
  //@PreAuthorize("denyAll")
  public void handlePersonSave(Product p) {
    // … you can now deal with Person in a type-safe way
    logger.warn("event product name = {}", p.getName());
  }

  @HandleBeforeLinkDelete
  public void checkLinkDeletePermission(Product project) {
    logger.warn("event product name = {}", project.getName());
  }

  @HandleBeforeDelete
  public void checkDeletePermission(Product project) {
    logger.warn("event product name = {}", project.getName());
  }
}