package oracle.apps.fltp.backend.domain.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

/**
 * Created by ecchang on 2016/10/19
 * resp for product
 */

@PreAuthorize("hasRole('USER')")
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
  @Override
  Product findOne(@Param("id") Long id);

  @Override
  List<Product> findAll();

  @Override
  List<Product> findAll(Sort sort);

  @Override
  List<Product> findAll(Iterable<Long> longs);

  @Override
  Page<Product> findAll(Pageable pageable);

}
