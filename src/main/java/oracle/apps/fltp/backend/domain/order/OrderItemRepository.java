package oracle.apps.fltp.backend.domain.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by ecchang on 2016/10/19
 */
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}
