package oracle.apps.fltp.backend.domain.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;

@RepositoryEventHandler(Userinfo.class)
@SuppressWarnings("unused")
public class UserinfoEventHandler {

    @Autowired
    private SettingsRepository settingsRepository;

    @HandleBeforeCreate
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void checkCreateAuthority(Userinfo User) {
    }

    @HandleAfterCreate
    public void createInitialSettings(Userinfo user) {
        Settings settings = new Settings();
        settings.setUser(user);
        settings.setType(Settings.SettingsType.LOCALE);
        settings.setValue("cn");
        settingsRepository.save(settings);
    }

    @HandleBeforeSave
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void checkUpdateAuthority(Userinfo User) {
    }

    @HandleBeforeDelete
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void checkDeleteAuthority(Userinfo User) {
    }

    @HandleBeforeLinkDelete
    @PreAuthorize("denyAll()")
    public void deleteCredentialForbidden(Userinfo User) {
        //deny all, cannot be called
    }
}
