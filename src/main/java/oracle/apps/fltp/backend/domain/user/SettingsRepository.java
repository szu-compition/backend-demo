package oracle.apps.fltp.backend.domain.user;

import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface SettingsRepository extends Repository<Settings, Long> {

    Settings save(Settings settings);

    List<Settings> findByUser_Email(String email);

    Settings findByTypeAndUser_Email(Settings.SettingsType type, String email);
}
