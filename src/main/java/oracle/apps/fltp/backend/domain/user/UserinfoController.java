package oracle.apps.fltp.backend.domain.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by ecchang on 2016/10/19
 * userinfo controller to get current user info
 */
@RestController
public class UserinfoController {
  @Autowired
  UserinfoRepository userRepo;

  @RequestMapping(value = "/api/user", method = RequestMethod.GET)
  public Principal getPrincipal(Principal user){
    return user;
  }
}
