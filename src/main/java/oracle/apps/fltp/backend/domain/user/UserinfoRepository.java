package oracle.apps.fltp.backend.domain.user;

import oracle.apps.fltp.backend.core.security.AuthUserDetailsService;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

/**
 * Created by ecchang on 2016/10/19
 * resp for userinfo
 *
 * Removed all Authorize check in this Repository
 * User info is required for UserDetailsService.
 * In UserDetailsService authorization suppose not finish yet
 * so it is not able to do Authorize check
 * This is a conflict
 * Make this repository initial lazy and inject lazy (inject is the key point)
 * {@link AuthUserDetailsService}
 */

@Lazy
public interface UserinfoRepository extends JpaRepository<Userinfo, Long> {

  @Override
  @PostAuthorize("returnObject?.email == authentication?.name or hasRole('ROLE_ADMIN')")
  Userinfo findOne(@Param("id") Long id);

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  List<Userinfo> findAll();

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  List<Userinfo> findAll(Sort sort);

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  List<Userinfo> findAll(Iterable<Long> longs);

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  Page<Userinfo> findAll(Pageable pageable);

  Page<Userinfo> findByLastName(@Param("lastName") String lastName, Pageable pageable);

  Userinfo findByEmail(@Param("email") String email);
}
