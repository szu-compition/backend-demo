package oracle.apps.fltp.backend.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.Valid;


/**
 * Created by ecchang on 2016/10/19
 * User entity
 */

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Userinfo {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Version
  private Integer version;

  @NotEmpty
  private String firstName;

  @NotEmpty
  private String lastName;

  @Column(unique = true)
  //@Email
  @NotEmpty
  private String email;

  private String roles;

  private String phoneNumber;

  @Valid
  @OneToOne(optional = false)
  @JoinColumn(name = "password_id")
  private PasswordStore password;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getRoles() {
    return roles;
  }

  public void setRoles(String roles) {
    this.roles = roles;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public PasswordStore getPassword() {
    return password;
  }

  public void setPassword(PasswordStore password) {
    this.password = password;
  }
}
