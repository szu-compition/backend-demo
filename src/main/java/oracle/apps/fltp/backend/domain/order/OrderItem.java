package oracle.apps.fltp.backend.domain.order;

import oracle.apps.fltp.backend.domain.product.Product;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Min;

/**
 * Created by ecchang on 2016/10/19
 */

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderItem {
  public enum ItemStatus{
    PENDING, OUT_OF_STOCK, ORDERED, PAYED, CANCEL, RETURNED, REFUNDED
  }

  public OrderItem(){}

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Version
  private Integer version;

  @OneToOne
  @JoinColumn()
  private Product product;

  @Min(1)
  private Integer quantity;

  @Enumerated(EnumType.STRING)
  private ItemStatus status;

  private Integer price;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public ItemStatus getStatus() {
    return status;
  }

  public void setStatus(ItemStatus status) {
    this.status = status;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }
}
