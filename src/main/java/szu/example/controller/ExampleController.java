package szu.example.controller;


import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import szu.example.Benz;

import java.util.Map;

@RestController
public class ExampleController {

  @Autowired
  Benz myBenz;
  @RequestMapping("/example/{para1}/{para2}")
  public Map<String, String> exampleCall(@PathVariable String para1, @PathVariable String para2){
    return ImmutableMap.of("message", "This is an example", "para1","echo" + para1, "para2", "echo" +para2, "myCar", myBenz.operator());
  }
}
