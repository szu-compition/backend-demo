package szu.example;

import org.springframework.stereotype.Component;

@Component()

public class Jack implements People {

  @Override
  public String whoami() {
    return "Jack";
  }
}
