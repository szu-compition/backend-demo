package szu.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Benz implements Car {

  @Autowired
  private People driver;

  @Override
  public String name() {
    return "Benz";
  }

  @Override
  public String operator() {
    return driver.whoami() + " is driving this " + name();
  }
}
