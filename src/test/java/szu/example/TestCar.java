package szu.example;

import oracle.apps.fltp.backend.ServerStarter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = ServerStarter.class)
@RunWith(SpringRunner.class)
public class TestCar {

  @Autowired
  Car car;

  @Test
  public void testBenz(){
    System.out.println(car.name());
    System.out.println(car.operator());
  }

}
