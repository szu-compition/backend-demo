# Backend-Demo

## Gradle
 Open Source Build Automation, similar as Maven
 [Getting Start](https://gradle.org/getting-started-gradle/)
 
## Spring Boot
 [Quick Start](http://projects.spring.io/spring-boot/)
 
 [Reference Guide](http://docs.spring.io/spring-boot/docs/1.4.2.RELEASE/reference/htmlsingle/)
 
 [Spring Framework Reference Guide](http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/)
 
 
## Spring Data
 [Quick Start](http://projects.spring.io/spring-data/#quick-start)
 [Common Reference](http://docs.spring.io/spring-data/commons/docs/current/reference/html/)